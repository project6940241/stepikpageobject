from .pages.product_page import ProductPage
import pytest


#setup для примера. Используется пля подготовки, выполнения проверки и удаления тестовых данных
@pytest.mark.add_to_basket
class TestAddToBasketFromProductPage():
    @pytest.fixture(scope="function", autouse=True)
    def setup(self):
        self.product = ProductFactory(title="Best book created by robot")
        self.link = self.product.link
        yield
        self.product.delete()
    def test_guest_cant_see_success_message(self, browser):

        link = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue/hacking-exposed-wireless_208/'
        page = ProductPage(browser, link, 0)
        page.open()
        page.should_not_be_success_message()


@pytest.mark.xfail
def test_guest_cant_see_success_message_after_adding_product_to_basket(browser):

    link = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue/hacking-exposed-wireless_208/'
    page = ProductPage(browser, link, 0)
    page.open()
    page.find_and_click_busket()
    page.should_not_be_success_message()

@pytest.mark.xfail
def test_message_disappeared_after_adding_product_to_basket(browser):

    link = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue/hacking-exposed-wireless_208/'
    page = ProductPage(browser, link,2)
    page.open()
    page.find_and_click_busket()
    page.should_be_dissapeared()