from .base_page import BasePage
from .locators import ProductPageLocators


class ProductPage(BasePage):
    def find_and_click_busket(self):
        link = self.browser.find_element(*ProductPageLocators.ADD_BUSKET)
        link.click()

    def check_price(self):
        link_Basket_Price = self.browser.find_element(*ProductPageLocators.BASKET_PRICE)
        link_BP = link_Basket_Price.text.split()[2]
        link_Message_Price = self.browser.find_element(*ProductPageLocators.MESSAGES_PRICE)
        link_MP = link_Message_Price.text
        assert link_BP == link_MP, "Prices DON'T MATCH"

    def check_name(self):
        link_Messages_Name = self.browser.find_element(*ProductPageLocators.MASSAGES_BOOKS)
        link_MN = link_Messages_Name.text
        link_Book_Name = self.browser.find_element(*ProductPageLocators.BOOK_NAME)
        link_BN = link_Book_Name.text
        assert link_MN == link_BN, "Names DON'T MATCH"

    def should_not_be_success_message(self):
        assert self.is_not_element_present(*ProductPageLocators.SUCCESS_MESSAGE), \
            "Success message is presented, but should not be"

    def should_be_dissapeared(self):
        assert self.is_dissapeared(*ProductPageLocators.DISSAPEAR_ELEMENT), \
            "Element is not dissappear =(("


