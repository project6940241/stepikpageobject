from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")
    BUCKET_ICON = (By.CSS_SELECTOR, 'span.btn-group>a.btn')
    ITEMS_BUCKET = (By.CSS_SELECTOR, 'basket-items')
    BUCKET_EMPTY = (By.CSS_SELECTOR, "#content_inner")

class LoginPageLocators:
    IS_ENTER_FORM = (By.CSS_SELECTOR, '#login_form')
    IS_REG_FORM = (By.CSS_SELECTOR, '#register_form')
    IS_MAIL_SK = (By.CSS_SELECTOR, "#id_registration-email")
    IS_PSSW_SK = (By.CSS_SELECTOR, "[id=id_registration-password1]")
    IS_PSSW_SK2 = (By.CSS_SELECTOR, "[id=id_registration-password2]")
    REGISTER_BUTTON = (By.CSS_SELECTOR, '[value=Register]') # "registration_submit"

# class Anekdot:
#     is_ded_moroz = (By.CSS_SELECTOR, '[title="Мем, Ymir"]')

class ProductPageLocators:
    ADD_BUSKET = (By.CSS_SELECTOR, 'button.btn-add-to-basket')
    MASSAGES_BOOKS = (By.CSS_SELECTOR, '#messages > div:nth-child(1) strong')
    BOOK_NAME = (By.CSS_SELECTOR,'div > h1')
    MESSAGES_PRICE = (By.CSS_SELECTOR, '#messages > div:last-child strong')
    BASKET_PRICE = (By.XPATH, "//div[strong = 'Basket total:']")
    SUCCESS_MESSAGE = (By.CSS_SELECTOR, '#messages > div:nth-child(2) strong')
    DISSAPEAR_ELEMENT = (By.CSS_SELECTOR, '#messages > div:nth-child(2) strong')

class BasePageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")
    LOGIN_LINK_INVALID = (By.CSS_SELECTOR, "#login_link_inc")
    USER_ICON = (By.CSS_SELECTOR, '.icon-user')