from .base_page import BasePage
from .locators import MainPageLocators

class BasketPage(BasePage):

    def should_not_product_in_basket(self):
        assert self.is_not_element_present(*MainPageLocators.ITEMS_BUCKET), "Products in bucket, but shouldn't"


    def should_basket_empty(self):
        assert self.is_text_in(*MainPageLocators.BUCKET_EMPTY, 'Your basket is empty.'), "There isn't basket empty"

