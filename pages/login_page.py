import time
from .base_page import BasePage
from .locators import LoginPageLocators


class LoginPage(BasePage):

    def should_be_login_page(self):
        self.should_be_login_form()
        self.should_be_register_form()
        self.should_be_login_url()

    def should_be_login_url(self):
        assert self.is_endpoint_has_element('login')

    def should_be_login_form(self):
        assert self.is_element_present(*LoginPageLocators.IS_ENTER_FORM), "ENTER Form is not presented"

    def should_be_register_form(self):
        assert self.is_element_present(*LoginPageLocators.IS_REG_FORM), "REGISTERED Form is not presented"

    def register_new_user(self, password='1a2s3d4f56789'):
        email = self.gen_mail()
        self.browser.find_element(*LoginPageLocators.IS_MAIL_SK).send_keys(email), 'Try to send email'
        self.browser.find_element(*LoginPageLocators.IS_PSSW_SK).send_keys(password), 'Try to send PSSW'
        self.browser.find_element(*LoginPageLocators.IS_PSSW_SK2).send_keys(password), 'Try to send PSSW2'
        reg_btn = self.browser.find_element(*LoginPageLocators.REGISTER_BUTTON)#, "REG_BUTTON DON.t work"
        reg_btn.click()
        self.should_be_authorized_user()
