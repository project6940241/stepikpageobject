from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.webdriver import WebDriver
import math
import time
from selenium.common.exceptions import NoAlertPresentException  # в начале файла
from selenium.common.exceptions import TimeoutException
from .locators import BasePageLocators
from .locators import MainPageLocators


class BasePage:
    def __init__(self, browser: WebDriver, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    def go_to_login_page(self):
        link = self.browser.find_element(*BasePageLocators.LOGIN_LINK)  # LOGIN_LINK_INVALID
        link.click()

    def should_be_login_link(self):
        assert self.is_element_present(*BasePageLocators.LOGIN_LINK), "Login link is not presented"

    def open(self):
        self.browser.get(self.url)

    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False

        return True

    def is_not_element_present(self, how, what, timeout=4):  # элемент не появляется
        try:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return True

        return False

    #  Если же мы хотим проверить,что какой-то элемент исчезает, то следует воспользоваться явным ожиданием вместе с
    #  функцией until_not, в зависимости от того, какой результат мы ожидаем:
    def is_dissapeared(self, how, what, timeout=4):  ## элемент исчезает
        try:
            WebDriverWait(self.browser, timeout). \
                until_not(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return False

        return True

    def is_endpoint_has_element(self, value):
        return True if value in self.browser.current_url else False

    # def wait_and_find_element(self, locator, time=10):
    #     return WebDriverWait(self.browser, time).until(EC.element_to_be_clickable((locator)))

    def solve_quiz_and_get_code(self):
        alert = self.browser.switch_to.alert
        x = alert.text.split(" ")[2]
        answer = str(math.log(abs((12 * math.sin(float(x))))))
        alert.send_keys(answer)
        alert.accept()
        try:
            alert = self.browser.switch_to.alert
            alert_text = alert.text
            print(f"Your code: {alert_text}")
            alert.accept()
        except NoAlertPresentException:
            print("No second alert presented")

    def is_text_in(self, how, what, text):
        return True if text in self.browser.find_element(how, what).text else False

    def go_to_busket(self):
        link = self.browser.find_element(*MainPageLocators.BUCKET_ICON)
        link.click()

    def should_be_authorized_user(self):
        assert self.is_element_present(*BasePageLocators.USER_ICON), "User icon is not presented," \
                                                                 " probably unauthorised user"
    def gen_mail(self):
        email = str(time.time()) + "@fakemail.org"
        return email

    # def send_keys_to(self, how, what, send):
    #     self.browser.find_element(how, what).send_keys(send)
