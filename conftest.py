
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options



def pytest_addoption(parser):  # Для запуска тестов из разных браузеров
    parser.addoption('--browser_name', action='store', default='chrome',
                     help="Choose browser: chrome or firefox")

    parser.addoption('--language', action='store', default='en')


@pytest.fixture(scope="function")
def browser(request):
    browser_name = request.config.getoption("browser_name")
    language = request.config.getoption('language')
    options = webdriver.ChromeOptions()
#    options.add_argument('--headless')
    options.add_argument("--start-maximized")
    options.add_experimental_option('prefs', {'intl.accept_languages': language})
    if browser_name == "chrome":
        print("\nstart chrome browser for test..")
        browser = webdriver.Chrome(options=options)
    elif browser_name == "firefox":
        print("\nstart firefox browser for test..")
        browser = webdriver.Firefox()
    else:
        raise pytest.UsageError("--browser_name should be chrome or firefox")
    yield browser
    print("\nquit browser..")
    browser.quit()

    # Запуск тестов из фаервокс или гугл:
    # pytest -s -v --browser_name=firefox 3lessons6_step5.py